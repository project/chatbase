CONTENTS OF THIS FILE
---------------------

 * Introduction
   * Features
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Chatbase.co offers AI-powered chatbots that streamline customer interaction. It
provides sophisticated automation, customization capabilities, 24/7 customer
support, and insights for business growth.

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/chatbase


FEATURES
--------

* Train the chatbot on the content of your website by automatically
  synchronizing your content with the chatbase.co service.

REQUIREMENTS
------------

 * You need to have a Secret Key.
 * You need to have a Chatbot ID.


INSTALLATION
------------

 * Use composer to download chatbase, which will download all the dependencies
 required by chatbase: `composer require drupal/chatbase`

CONFIGURATION
-------------

    1. Direct your browser to admin/config/services/chatbase to configure the
       module.

    2. You will need to put in your Secret Key and Chatbot ID.
