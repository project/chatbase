<?php

namespace Drupal\chatbase\Service;

use Drupal\chatbase\Client\ChatbaseApiClientInterface;
use Drupal\chatbase\Client\ChatbaseApiResponse;

/**
 * Implements ChatBot Manager service.
 */
class ChatbotManager implements ChatbotManagerInterface {

  /**
   * The chatbase api client.
   *
   * @var \Drupal\chatbase\Client\ChatbaseApiClientInterface
   */
  protected $client;

  /**
   * Constructs a new ChatbotManager object.
   *
   * @param \Drupal\chatbase\Client\ChatbaseApiClientInterface $client
   *   The chatbase api client.
   */
  public function __construct(ChatbaseApiClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function createChatbot(string $chatbot_name, $source): ChatbaseApiResponse {
    $options = [
      'chatbotName' => $chatbot_name,
    ];
    if (is_string($source)) {
      $options['sourceText'] = $source;
    }
    elseif (is_array($source)) {
      $options['urlsToScrape'] = $source;
    }
    return $this->client->request(static::SOURCE_CHATBOT_CREATE, 'POST', ['json' => $options]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateChatbot(string $chatbot_id, string $chatbot_name, $source): ChatbaseApiResponse {
    $options = [
      'chatbotId' => $chatbot_id,
      'chatbotName' => $chatbot_name,
    ];
    if (is_string($source)) {
      $options['sourceText'] = $source;
    }
    elseif (is_array($source)) {
      $options['urlsToScrape'] = $source;
    }
    return $this->client->request(static::SOURCE_CHATBOT_UPDATE, 'POST', ['json' => $options]);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteChatbot(string $chatbot_id): ChatbaseApiResponse {
    $options = [
      'query' => [
        'chatbotId' => $chatbot_id,
      ],
    ];
    return $this->client->request(static::SOURCE_CHATBOT_DELETE, 'DELETE', $options);
  }

}
