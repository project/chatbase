<?php

namespace Drupal\chatbase\Service;

use Drupal\chatbase\Client\ChatbaseApiResponse;

/**
 * Implements ChatBot Manager Interface.
 */
interface ChatbotManagerInterface {

  /**
   * Endpoint source path to create a chatbot.
   *
   * @var string
   */
  const SOURCE_CHATBOT_CREATE = 'create-chatbot';

  /**
   * Endpoint source path to update a chatbot.
   *
   * @var string
   */
  const SOURCE_CHATBOT_UPDATE = 'update-chatbot-data';

  /**
   * Endpoint source path to delete a chatbot.
   *
   * @var string
   */
  const SOURCE_CHATBOT_DELETE = 'delete-chatbot';

  /**
   * Create a ChatBot.
   *
   * @param string $chatbot_name
   *   The name of the chatbot to be created.
   * @param string|array $source
   *   The text data for the chatbot or an array of URLs to be scraped for text
   *   content by Chatbase.
   *
   * @return \Drupal\chatbase\Client\ChatbaseApiResponse
   *   The Api response result. ChatbaseApiResponse::success() should be used to
   *   indicate that response successful or ChatbaseApiResponse::failure() to
   *   indicate that the response failed.
   */
  public function createChatbot(string $chatbot_name, $source): ChatbaseApiResponse;

  /**
   * Update a ChatBot.
   *
   * @param string $chatbot_id
   *   A unique identifier for the chatbot.
   * @param string $chatbot_name
   *   The new name for the chatbot.
   * @param string|array $source
   *   The new source text to update the chatbot or an array of URLs to scrape
   *   for text content to update the chatbot with the scraped data.
   *
   * @return \Drupal\chatbase\Client\ChatbaseApiResponse
   *   The Api response result. ChatbaseApiResponse::success() should be used to
   *   indicate that response successful or ChatbaseApiResponse::failure() to
   *   indicate that the response failed.
   */
  public function updateChatbot(string $chatbot_id, string $chatbot_name, $source): ChatbaseApiResponse;

  /**
   * Delete a ChatBot.
   *
   * @param string $chatbot_id
   *   A unique identifier for the chatbot that you want to delete.
   *
   * @return \Drupal\chatbase\Client\ChatbaseApiResponse
   *   The Api response result. ChatbaseApiResponse::success() should be used to
   *   indicate that response successful or ChatbaseApiResponse::failure() to
   *   indicate that the response failed.
   */
  public function deleteChatbot(string $chatbot_id): ChatbaseApiResponse;

}
