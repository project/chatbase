<?php

namespace Drupal\chatbase\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Chatbase Dashboard routes.
 */
class DashboardController extends ControllerBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new DashboardController object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * Builds the Dashboard response.
   */
  public function build() {
    $data = $this->state->get('chatbase_conversations_history');

    $rows = [];
    if ($data && ($data = Json::decode($data))) {
      $conversations = $data['conversations'] ?? [];
      foreach ($conversations as $conversation) {
        $messages = $conversation['messages'] ?? [];
        if (empty($messages)) {
          continue;
        }
        $date = new DrupalDateTime($conversation['created_at']);
        $row = [
          'date' => $date->format('Y-m-d H:i:s'),
          'customer' => [],
          'assistant' => [],
          'operations' => [],
        ];
        $found = FALSE;
        // Find first message from customer.
        foreach ($messages as $delta => $message) {
          if ($message['role'] === 'user') {
            $row['customer'] = ['data' => ['#markup' => $message['content']]];
            $found = $delta;
            break;
          }
        }
        // Message from customer was found now we want answer from assistant.
        if ($found !== FALSE) {
          $messages = array_slice($messages, $found + 1);
          foreach ($messages as $message) {
            if ($message['role'] === 'assistant') {
              $row['assistant'] = ['data' => ['#markup' => $message['content']]];
              break;
            }
          }
        }
        $row['operations']['data'] = [
          '#type' => 'operations',
          '#links' => [
            'view' => [
              'title' => $this->t('View'),
              'url' => Url::fromRoute('chatbase.conversation.view', ['id' => $conversation['id']]),
              'attributes' => [
                'class' => ['btn', 'use-ajax'],
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode([
                  'width' => 600,
                  'height' => 'auto',
                ]),
              ],
            ],
          ],
          '#attached' => [
            'library' => ['core/drupal.dialog.ajax'],
          ],
        ];

        $rows[] = $row;
      }
    }

    $build['content'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Date'),
        $this->t('Customer'),
        $this->t('Bot'),
        $this->t('Operations'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('There are no conversations yet.'),
    ];

    return $build;
  }

  /**
   * View conversation history.
   *
   * @param string $id
   *   Conversation ID.
   */
  public function viewConversation(string $id) {
    $data = $this->state->get('chatbase_conversations_history');

    $content = [
      '#theme' => 'chatbase_conversation',
      '#attached' => [
        'library' => ['chatbase/conversation'],
      ],
    ];
    if ($data && ($data = Json::decode($data))) {
      $conversations = $data['conversations'] ?? [];
      foreach ($conversations as $conversation) {
        if ($conversation['id'] !== $id) {
          continue;
        }
        $messages = $conversation['messages'] ?? [];
        $content['#messages'] = $messages;
        $content['#title'] = $this->t('Source: @source', ['@source' => $conversation['source']]);
        break;
      }
    }
    return $content;
  }

}
