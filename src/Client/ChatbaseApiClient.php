<?php

namespace Drupal\chatbase\Client;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

/**
 * Implements Chatbase API Client.
 */
class ChatbaseApiClient implements ChatbaseApiClientInterface {

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The chatbase config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new ChatbaseApiClient object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Logger.
   */
  public function __construct(ClientInterface $client, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    $this->httpClient = $client;
    $this->config = $config_factory->get('chatbase.settings');
    $this->logger = $logger;
  }

  /**
   * Gets the API Secret Key.
   *
   * @return string
   *   Secret Key.
   */
  private function getApiKey(): string {
    return $this->config->get('secret_key');
  }

  /**
   * Gets the authorization token.
   *
   * @return string
   *   Token.
   */
  private function getAuthorizationToken(): string {
    return sprintf('Bearer %s', $this->getApiKey());
  }

  /**
   * {@inheritdoc}
   */
  public function request(string $resource, string $method = 'GET', array $options = []): ChatbaseApiResponse {
    $request_options = [
      RequestOptions::HEADERS => [
        'accept' => 'application/json',
        'Authorization' => $this->getAuthorizationToken(),
      ],
    ];
    $request_options = NestedArray::mergeDeep($request_options, $options);
    $request_uri = sprintf('%s/%s', static::API_ENDPOINT, $resource);

    try {
      $response = $this->httpClient->request($method, $request_uri, $request_options);
      $result = Json::decode($response->getBody());
      if ($response->getStatusCode() === 200) {
        return ChatbaseApiResponse::success($result);
      }
      $reason = $result['message'] ?? NULL;
      $code = $response->getStatusCode();
      return ChatbaseApiResponse::failure($reason, $code);
    }
    catch (ClientException | ServerException $e) {
      $response = $e->getResponse();
      $result = Json::decode($response->getBody());
      $reason = $result['message'] ?? NULL;
      $code = $response->getStatusCode();
      return ChatbaseApiResponse::failure($reason, (string) $code);
    }
    catch (\Throwable $e) {
      $this->logger->error('Caught exception: @msg. Trace: @trace', [
        '@msg' => $e->getMessage(),
        '@trace' => $e->getTraceAsString(),
      ]);
      return ChatbaseApiResponse::failure('Internal error. Please contact site administrator.');
    }
  }

}
