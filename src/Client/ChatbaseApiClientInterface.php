<?php

namespace Drupal\chatbase\Client;

/**
 * The Chatbase API client interface.
 */
interface ChatbaseApiClientInterface {

  /**
   * Endpoint path for API.
   *
   * @var string
   */
  const API_ENDPOINT = 'https://www.chatbase.co/api/v1';

  /**
   * API request.
   *
   * @param string $resource
   *   The request api resource.
   * @param string $method
   *   The request method.
   * @param array $options
   *   The request options.
   *
   * @return \Drupal\chatbase\Client\ChatbaseApiResponse
   *   The response result. ChatbaseApiResponse::success() should be used to
   *   indicate that response successful or ChatbaseApiResponse::failure() to
   *   indicate that the response failed.
   */
  public function request(string $resource, string $method = 'GET', array $options = []): ChatbaseApiResponse;

}
