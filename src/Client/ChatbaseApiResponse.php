<?php

namespace Drupal\chatbase\Client;

use Drupal\Component\Render\MarkupInterface;

/**
 * Provides a value object representing the "response" from api.
 */
final class ChatbaseApiResponse {

  /**
   * The api response state.
   *
   * @var bool|null
   */
  protected $state;

  /**
   * The api response result.
   *
   * @var mixed|null
   */
  protected $result;

  /**
   * The api response result "code".
   *
   * @var mixed|null
   */
  protected $code;

  /**
   * Constructs a new ChatbaseApiResponse object.
   *
   * @param bool $state
   *   TRUE if api response successful, otherwise FALSE.
   * @param mixed|null $result
   *   The response result or reason why request failed.
   * @param mixed|null $code
   *   (optional) The "code" explaining response result.
   */
  public function __construct(bool $state, $result = NULL, $code = NULL) {
    assert(is_null($result) || is_string($result) || is_array($result) || $result instanceof MarkupInterface);
    assert(is_null($code) || is_string($code) || is_int($code));
    $this->state = $state;
    $this->result = $result;
    $this->code = $code;
  }

  /**
   * Constructs a success response result.
   *
   * @param mixed $result
   *   Response result.
   *
   * @return static
   */
  public static function success($result): ChatbaseApiResponse {
    return new static(TRUE, $result);
  }

  /**
   * Constructs a failure response result.
   *
   * @param mixed|null $reason
   *   (optional) The reason why the request failed.
   * @param mixed|null $code
   *   (optional) The "code" explaining the response result.
   *
   * @return static
   */
  public static function failure($reason = NULL, $code = NULL): ChatbaseApiResponse {
    return new static(FALSE, $reason, $code);
  }

  /**
   * Gets the "code".
   *
   * @return mixed|null
   *   The "code" for this response result, NULL when not provided.
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Gets the "result".
   *
   * @return mixed|null
   *   The "result" for this response, NULL when not provided.
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * Determines whether the response is "success".
   *
   * @return bool
   *   Whether the response is "success".
   */
  public function isSuccess(): bool {
    return $this->state === TRUE;
  }

  /**
   * Determines whether the response is "failure".
   *
   * @return bool
   *   Whether the response is "failure".
   */
  public function isFailure(): bool {
    return $this->state === FALSE;
  }

}
