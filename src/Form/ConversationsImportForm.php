<?php

namespace Drupal\chatbase\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Chatbase Conversations import form.
 */
class ConversationsImportForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new ConversationsImportForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, StateInterface $state) {
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chatbase_conversations_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['json'] = [
      '#type' => 'file',
      '#title' => $this->t('Choose a file'),
      '#description' => $this->t('Conversation history json file can be obtained from <a href="https://www.chatbase.co/chatbot/*/dashboard">https://www.chatbase.co/chatbot/*/dashboard</a>.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
        'file_validate_size' => [Environment::getUploadMaxSize()],
      ],
      '#upload_location' => 'temporary://',
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile[] $all_files */
    $all_files = $this->getRequest()->files->get('files', []);
    if (empty($all_files['json'])) {
      $form_state->setErrorByName('json', $this->t('No JSON file was provided.'));
    }
    elseif (!$all_files['json']->isValid()) {
      $form_state->setErrorByName('json', $this->t('The provided JSON file is invalid.'));
    }
    else {
      $file = file_save_upload('json', $form['json']['#upload_validators'], $form['json']['#upload_location'], 0, FileSystemInterface::EXISTS_REPLACE);

      if (!$file) {
        $form_state->setErrorByName('json', $this->t('An error occurred while trying to upload the JSON file, please try again later.'));
      }
      else {
        $form_state->set('json_file', $file->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    /** @var \Drupal\file\FileInterface $file */
    $file = $file_storage->load($form_state->get('json_file'));
    $json = file_get_contents($file->getFileUri());
    $this->state->set('chatbase_conversations_history', $json);
    // Remove uploaded file, we do not want to store it.
    $file->delete();
    $this->messenger()
      ->addStatus($this->t('Conversations history successfully imported.'));
  }

}
