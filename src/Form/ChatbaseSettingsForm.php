<?php

namespace Drupal\chatbase\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Chatbase settings for this site.
 */
class ChatbaseSettingsForm extends ConfigFormBase {

  /**
   * The chatbot manager.
   *
   * @var \Drupal\chatbase\Service\ChatbotManagerInterface
   */
  protected $chatbotManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->chatbotManager = $container->get('chatbase.chatbot.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chatbase_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['chatbase.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('chatbase.settings');

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $config->get('secret_key'),
      '#required' => TRUE,
    ];
    $form['chatbot_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chatbot Name'),
      '#default_value' => $config->get('chatbot_name'),
      '#required' => TRUE,
    ];
    $form['chatbot_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chatbot ID'),
      '#default_value' => $config->get('chatbot_id'),
      '#attributes' => [
        'disabled' => 'disabled',
      ],
    ];
    if (!$config->get('chatbot_id')) {
      $form['chatbot_source'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Chatbot Source'),
        '#description' => $this->t('The text data for the chatbot.'),
      ];
    }

    $form['chatbot_embed'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('ChatBot embed'),
    ];
    $form['chatbot_embed']['chatbot_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable ChatBot'),
      '#description' => $this->t('Add a chat bubble to the bottom of your website.'),
      '#default_value' => $config->get('chatbot_enabled'),
    ];
    $form['chatbot_embed']['chatbot_exclude_admin_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude from Admin theme'),
      '#default_value' => $config->get('chatbot_exclude_admin_theme'),
      '#states' => [
        'visible' => [
          ':input[name="chatbot_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (isset($form['chatbot_source'])) {
      $source = $form_state->getValue('chatbot_source');
      if (strlen($source) < 100) {
        $form_state->setErrorByName('chatbot_source', $this->t('Chatbot source text should not be less than 100 characters.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('chatbase.settings');
    $config
      ->set('secret_key', $form_state->getValue('secret_key'))
      ->set('chatbot_name', $form_state->getValue('chatbot_name'))
      ->set('chatbot_id', $form_state->getValue('chatbot_id'))
      ->set('chatbot_enabled', $form_state->getValue('chatbot_enabled'))
      ->set('chatbot_exclude_admin_theme', $form_state->getValue('chatbot_exclude_admin_theme'))
      ->save();

    if ($form_state->isValueEmpty('chatbot_id')) {
      $result = $this->chatbotManager->createChatbot(
        $config->get('chatbot_name'),
        $form_state->getValue('chatbot_source')
      );
      if ($result->isSuccess() && ($chatbot_id = $result->getResult()['chatbotId'] ?? FALSE)) {
        $config->set('chatbot_id', $chatbot_id)->save();
      }
    }
    parent::submitForm($form, $form_state);
  }

}
